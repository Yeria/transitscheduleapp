package com.yeria.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.yeria.helpers.models.Stop;

import org.w3c.dom.Comment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Phillip on 1/26/2016.
 */
public class StopListDAO {
    // Database fields
    private SQLiteDatabase database;
    private StopListDbHelper dbHelper;
    private String[] allColumns = { StopListDbHelper.STOP_ID,
            StopListDbHelper.STOP_NAME };

    public StopListDAO(Context context) {
        dbHelper = new StopListDbHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void insertAllStops(List<Stop> stopList) {
        for (Stop s : stopList) {
            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(StopListDbHelper.STOP_ID, s.getId());
            values.put(StopListDbHelper.STOP_NAME, s.getName());

            database.insert(StopListDbHelper.TABLE_NAME, null, values);
        }
    }

//    public void deleteComment(Comment comment) {
//        long id = comment.getId();
//        System.out.println("Comment deleted with id: " + id);
//        database.delete(StopListDbHelper.TABLE_COMMENTS, StopListDbHelper.COLUMN_ID
//                + " = " + id, null);
//    }

    public List<Stop> getAllStops() {
        List<Stop> stops = new ArrayList<Stop>();

        Cursor cursor = database.query(StopListDbHelper.TABLE_NAME,
                allColumns, null, null, null, null, StopListDbHelper.STOP_NAME);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Stop stop = cursorToStop(cursor);
            stops.add(stop);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return stops;
    }

    private Stop cursorToStop(Cursor cursor) {
        Stop st = new Stop();
        st.setId(cursor.getString(0));
        st.setName(cursor.getString(1));
        return st;
    }
}
