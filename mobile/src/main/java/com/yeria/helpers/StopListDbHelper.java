package com.yeria.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.yeria.helpers.models.Stop;

import java.util.List;

/**
 * Created by Phillip on 1/10/2016.
 */
    public class StopListDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "TransitSchedule.db";
    public static final String TABLE_NAME = "stop_list";
    public static final String STOP_ID = "stop_id";
    public static final String STOP_NAME = "stop_name";
    private static final String TABLE_CREATE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    STOP_ID + " TEXT, " +
                    STOP_NAME + " TEXT);";

    public StopListDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

//    public void initialize(StopListDbHelper slDbHelper, Map<String, String> stopList){
//        // Gets the data repository in write mode
//        SQLiteDatabase db = slDbHelper.getWritableDatabase();
//
//        for ( Map.Entry<String, String> kvp : stopList.entrySet() ) {
//            // Create a new map of values, where column names are the keys
//            ContentValues values = new ContentValues();
//            values.put(STOP_ID, kvp.getKey());
//            values.put(STOP_NAME, kvp.getValue());
//
//            db.insert(TABLE_NAME, null, values);
//        }
//    }

    public void initialize(StopListDbHelper slDbHelper, List<Stop> stopList){
        // Gets the data repository in write mode
        SQLiteDatabase db = slDbHelper.getWritableDatabase();

        for (Stop s : stopList) {
            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(STOP_ID, s.getId());
            values.put(STOP_NAME, s.getName());

            db.insert(TABLE_NAME, null, values);
        }
    }

    public String[] getStopDetail(){
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        String[] data = null;
        if (cursor.moveToFirst()) {
            do {
                // get  the  data into array,or class variable
            } while (cursor.moveToNext());
        }
        db.close();
        return data;
    }
}
