package com.yeria.helpers.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phillip on 1/10/2016.
 */
public class Stop {
    @SerializedName("StopID")
    private String id;
    @SerializedName("StopName")
    private String name;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}