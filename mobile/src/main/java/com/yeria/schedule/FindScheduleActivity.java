package com.yeria.schedule;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;

import com.yeria.helpers.StopListDAO;
import com.yeria.helpers.StopListDbHelper;
import com.yeria.helpers.models.Stop;
import com.yeria.transitschedule.R;

import java.util.List;

/**
 * Created by Phillip on 1/10/2016.
 */
public class FindScheduleActivity extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_schedule);
        StopListDAO dbHelper = new StopListDAO(getApplicationContext());
        dbHelper.open();
        List<Stop> stops = dbHelper.getAllStops();
        //ToDo: Get all stop description (STOP_NAMES) and turn it into String[] and test it with Adaptor
        String[] startItems = new String[stops.size()];
        String[] endItems = new String[stops.size()];

        for (int i = 0; i < stops.size(); i ++){
            startItems[i] = stops.get(i).getName();
            endItems[i] = stops.get(i).getName();
        }
        //String[] startItems = new String[]{"Cooksville GO", "Dixie GO", "Union Station"};
        //String[] startItems = dbHelper.getStopDetail();
        //String[] endItems = new String[]{"Union Station", "Milton GO"};

        final AutoCompleteTextView actvStart = (AutoCompleteTextView)findViewById(R.id.actvStart);
        final AutoCompleteTextView actvEnd = (AutoCompleteTextView)findViewById(R.id.actvEnd);

        ArrayAdapter<String> actvStartAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, startItems);
        ArrayAdapter<String> actvEndAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, endItems);

        actvStart.setAdapter(actvStartAdapter);
        actvEnd.setAdapter(actvEndAdapter);

        actvStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                actvStart.showDropDown();
            }
        });
        actvEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                actvEnd.showDropDown();
            }
        });

        //autoCompleteTextView.setOnItemClickListener(new OnItemClickListener() {
        //    public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
        //        String selection = (String)parent.getItemAtPosition(position);
        //        //TODO Do something with the selected text
        //    }
        //});

        Button b = (Button)findViewById(R.id.btnSearch);
        b.setOnClickListener(new OnClickListener(){
            public void onClick(View v) {
                char[] startSel = actvStart.getText().toString().toCharArray();
                char[] endSel = actvEnd.getText().toString().toCharArray();
                TextView t = (TextView)findViewById(R.id.srcResult);
                t.setText(startSel, 0, startSel.length);
                //ImageView iv = (ImageView) findViewById(R.id.imageview1);
                //iv.setVisibility(View.VISIBLE);
            }
        });
    }
}
