package com.yeria.schedule;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.yeria.helpers.StopListDAO;
import com.yeria.helpers.models.Stop;
import com.yeria.helpers.StopListDbHelper;
//import com.yeria.transit.tasks.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.*;
import com.yeria.transitschedule.R;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String[] urls = {"http://10.0.2.2/RouteService.svc/GetAllStops"};

        // Will eventually need to add a check for whether 1. data from service source got updated, and 2. has internet connection
        AsyncTask<String, Void, InputStream> task = new RetrieveAllStops().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, urls);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Create floating action button
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, FindScheduleActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class RetrieveAllStops extends AsyncTask<String, Void, InputStream> {
        protected InputStream doInBackground(String... urls) {
            int timeout = 3000;
            HttpURLConnection c = null;
            try {
                URL u = new URL(urls[0]);
                c = (HttpURLConnection) u.openConnection();
                c.setRequestMethod("GET");
                c.setRequestProperty("Content-length", "0");
                c.setUseCaches(false);
                c.setAllowUserInteraction(false);
                c.setConnectTimeout(timeout);
                c.setReadTimeout(timeout);
                c.connect();
                int status = c.getResponseCode();

                switch (status) {
                    case 200:
                    case 201:
                        StopListDAO dbHelper = new StopListDAO(getApplicationContext());
                        dbHelper.open();
                        //StopListDbHelper slDbHelper = new StopListDbHelper(getBaseContext());
                        Gson gson = new Gson();
                        Reader reader = new InputStreamReader(c.getInputStream());
                        Stop[] gsonObj = gson.fromJson(reader, Stop[].class);
                        List<Stop> stops = Arrays.asList(gsonObj);
                        dbHelper.insertAllStops(stops);
                        dbHelper.close();
                        //slDbHelper.initialize(slDbHelper, stops);
                        return c.getInputStream();
//                        BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
//                        StringBuilder sb = new StringBuilder();
//                        String line;
//                        while ((line = br.readLine()) != null) {
//                            sb.append(line + "\n");
//                        }
//                        br.close();
//                        return sb.toString();
                }

            } catch (MalformedURLException ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (c != null) {
                    try {
                        c.disconnect();
                    } catch (Exception ex) {
                        Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            return null;
        }

        protected void onPostExecute(InputStream result) {
//            StopListDbHelper slDbHelper = new StopListDbHelper(getBaseContext());
//            Gson gson = new Gson();
//            Reader reader = new InputStreamReader(result);
//            Stop[] gsonObj = gson.fromJson(reader, Stop[].class);
//            List<Stop> stops = Arrays.asList(gsonObj);
//            slDbHelper.initialize(slDbHelper, stops);
        }
    }
}
